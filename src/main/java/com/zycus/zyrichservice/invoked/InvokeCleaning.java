package com.zycus.zyrichservice.invoked;

import org.apache.commons.lang.StringUtils;

import routines.Zyrich;

public class InvokeCleaning {

//	public static void main(String[] args) throws IOException {
////		BufferedReader br = new BufferedReader(new FileReader(
////				"E:/supplier_test.txt"));
//		String s1 = "team industrial services netherlands";
//		List<String> cleanedVendorList = new ArrayList<String>();
//
//		while ((s1 = br.readLine()) != null) {
//			s1 = Zyrich.replaceCharwithSpace(s1);
//			s1 = Zyrich.removingTrailingNumbers(s1);
//			s1 = Zyrich.cleansedName(s1);
//			s1 = Zyrich.removingTrailingNumbers(s1);
//			s1 = Zyrich.replaceWordwithOtherWord(s1);
//			s1 = Zyrich.replaceWordwithSpace(s1);
//			cleanedVendorList.add(s1.toUpperCase());
//		}
//
//		FileWriter fr = new FileWriter("E:/supplier_test_Output.txt");
//		for (String string : cleanedVendorList) {
//			fr.write(string + "\n");
//		}
//		fr.close();
//		br.close();
//	}

	public static String cleanVendorName(String name) {

		if (StringUtils.isNotBlank(name) && !(name.trim().equals(""))) {
//			name = Zyrich.replaceEscapeChars(name);
			name = Zyrich.replaceCharwithSpace(name);
			name = Zyrich.removingTrailingNumbers(name);
			name = Zyrich.cleansedName(name);
			name = Zyrich.removingTrailingNumbers(name);
			name = Zyrich.replaceWordwithOtherWord(name);
			name = Zyrich.replaceWordwithSpace(name);
			return name;
		}
		return name;
	}
}
