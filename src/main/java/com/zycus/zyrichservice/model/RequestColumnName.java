package com.zycus.zyrichservice.model;

import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

import com.zycus.zyrich.atoms.IndexConfig;
import com.zycus.zyrich.dao.IndexConfigDAO;
import com.zycus.zyrich.exceptions.ZyrichException;

public class RequestColumnName {
	
	private static String columnNames;
	
	private static IndexConfig indexConfig ;
	
	public static String getColumnNames(String repository) throws ZyrichException {
		if(StringUtils.isBlank(columnNames)) {
		IndexConfigDAO indexConfigDAO = new IndexConfigDAO();
		columnNames= indexConfigDAO.getColumnLableNames(repository);
		}
		
		return columnNames;
	}
	
	public static IndexConfig getIndexConfig(HashMap<String, String> map) throws ZyrichException{
		if(null==indexConfig) {
			// APP is only for one repo
		 indexConfig = new IndexConfigDAO()
				.getIndex(map.get("indexname"));
		}
		
		return indexConfig;
	}

}
